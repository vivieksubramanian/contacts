//key-valataStore
package com.contacts.datastore;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.stream.Stream;

import org.json.JSONObject;

public class DataStore {

	public static HashMap<String, Integer> dataStore = new HashMap<>();
	public static String message = "";
	public static int lineNo = 0;
	public static String home = System.getProperty("user.home");
	public static String mainDirectory = home + File.separator + "FreshWorks";
	public static String defaultDataDirectory = mainDirectory;

	public static void createData(File file) throws Exception {

		JSONObject json = DataStore.getDetails(new Scanner(System.in));
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(file, true);
			fileWriter.append(json.toString() + "\n");
			fileWriter.flush();
			//log(json+" check");
			Iterator itr = json.keys();
			while (itr.hasNext()) {
				String key = (String) itr.next();
				dataStore.put(key, ++lineNo);
			}
		} finally {
			fileWriter.close();
		}
	}

	public static JSONObject getDetails(Scanner scanner) throws Exception {

		log("\nEnter the key :\n");

		Details details = new Details();

		String key = scanner.next();
		if (key.length() > 32 || key.length() < 1) {
			message = "You exceeded the Key length limit for your reference length limit is 32 characters";
			throw new Exception(message);
		} else if (dataStore.containsKey(key)) {
			message = "Key is already in dataStore try some other key";
			throw new Exception(message);
		}

		details.setKey(key);
		log("\nMobile number : \n");
		details.setMobileNumber(scanner.nextLong());
		details.setCreation_time(System.currentTimeMillis());
		log("\nSet TTL (y|n)\n");
		if (scanner.next().charAt(0) == 'y') {
			log("\nTime To Live (secs): \n");
			details.setTime_to_live(scanner.nextLong());
		}

		JSONObject mainJson = new JSONObject();

		JSONObject value = new JSONObject();
		value.put("MOBNUM", new Long(details.getMobileNumber()));
		value.put("CRTTIME", new Long(details.getCreation_time()));
		value.put("TTL", new Long(details.getTime_to_live()));

		mainJson.put(details.getKey(), value);
		return mainJson;
	}
	
	//read operation
	public static void readData(File file, String key) throws IOException {



		String data = null;
		if (file.length() == 0) {
			log("\nDataStore is Empty...\n");
		} else if(dataStore.containsKey(key)){
			try (Stream<String> lines = Files.lines(Paths.get(file.getAbsolutePath()))) {
				data = lines.skip((dataStore.get(key) - 1)).findFirst().get();
			}

			JSONObject json = new JSONObject(data);
			JSONObject valueJson = json.getJSONObject(key);
			if (valueJson.getLong("TTL") != 0 && isExpired(System.currentTimeMillis(), valueJson)) {
				log("No contact found..");
				// call delete fn with this key here
				deleteData(file, key);
			} else {
				log("\n"+json.get(key).toString()+"\n");
			}
		} else {
			log("\nContact not found...\n");
		}

	}

	private static boolean isExpired(long currentTimeMillis, JSONObject json) {
		long diffTime = (currentTimeMillis/1000) - (json.getLong("CRTTIME") / 1000);
		//log(diffTime+"diffTym\n");
		if (diffTime > json.getLong("TTL")) {
			return true;
		}
		return false;
	}

	//delete operation
	public static void deleteData(File file, String key) throws IOException {
		//log(file.getAbsolutePath());
		if(file.length() == 0) {
			log("\nDataStore is Empty...\n");
		}else if(dataStore.containsKey(key)) {
			String line = null; 
			try (Stream<String> lines = Files.lines(Paths.get(file.getAbsolutePath()))) {
				line = lines.skip((dataStore.get(key) - 1)).findFirst().get();
			}
			deleteThisLine(line, file);
			dataStore.clear();
			initializeDataStore(file.getPath());
			log("deletion successfull");
		} else {
			log("Invalid key...");
		}


	}

	private static void deleteThisLine(String line, File file) throws IOException {
		File tempFile = new File(mainDirectory +File.separator+"myTempFile.txt");
		tempFile.createNewFile();


		BufferedReader reader = new BufferedReader(new FileReader(file));
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

		String lineToRemove = line;
		String currentLine;

		while((currentLine = reader.readLine()) != null) {
			// trim newline when comparing with lineToRemove
			String trimmedLine = currentLine.trim();
			if(trimmedLine.equals(lineToRemove)) continue;
			writer.write(currentLine + System.getProperty("line.separator"));
		}
		writer.close(); 
		reader.close(); 
		if(!tempFile.renameTo(file)) {
			log("\nrename denied\n");
		}

	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		try {
			//log("Main data dir: "+mainDirectory+"----------\n");
			createMainDirectory();
			createInfoFile(scanner);
			createDataDirectory();
			File file = checkAndCreateDataStoreIfNotExist();
			initializeDataStore(file.getPath());
			log("########################### CONTACTS #############################\n\n");

			while (true) {
				log("1. Create contact\n2. Get contact\n3. Delete contact\n4. Exit\n\nEnter your choice\n");
				int choice = scanner.nextInt();
				switch (choice) {
				case 1:
					try {
						DataStore.createData(file);
						log("\nContact saved...\n");
					} catch (Exception e) {
						e.printStackTrace();
						log("Coundn't save contact...");
					}
					break;
				case 2:
					log("Search Name: \n");
					try {
						String key = scanner.next();
						DataStore.readData(file, key);
					} catch (Exception e) {
						e.printStackTrace();
						log("Couldn't read data...");
					}
					break;
				case 3:
					log("Enter key:\n");
					try {

						String key = scanner.next();
						//log(file.getAbsolutePath()+"\n");

						DataStore.deleteData(file, key);
					} catch (Exception e) {
						log("Unable to delete contact");
						e.printStackTrace();
					}
					break;
				case 4:
					log("Bye!");
					return;
				default:
					log("invalid choice.\n");
					continue;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			scanner.close();
		}
	}

	private static void createInfoFile(Scanner scanner) throws Exception {

		File infoFile = new File(mainDirectory + File.separator + "path.info");
		//log("Is infoFile exists: "+infoFile.exists()+"\n");
		if (!infoFile.exists()) {
			infoFile.createNewFile();
			log("Set data path (y|n)\n");
			if (scanner.next().charAt(0) == 'y') {
				String dataDirPath = scanner.next();
				FileWriter fileWriter = null;
				try {
					fileWriter = new FileWriter(infoFile);
					fileWriter.append(dataDirPath+File.separator+"FreshWorks");
					fileWriter.flush();
				} finally {
					fileWriter.close();
				}
				defaultDataDirectory = dataDirPath + File.separator + "FreshWorks";
			}
		} else {
			String dataDirPath = readFile(infoFile.getAbsolutePath());
			//log("default data dir path: "+dataDirPath + File.separator+"\n");
			defaultDataDirectory = dataDirPath;
		}

	}

	private static void createDataDirectory() {
		File dataDir = new File(defaultDataDirectory);
		//log("Is data directory exists: "+dataDir.exists()+" defaultDataDirectory: "+defaultDataDirectory+" Pathinfile: "+dataDir.getAbsolutePath()+"\n" );
		if (!dataDir.exists()) {
			dataDir.mkdirs();
		}

	}

	private static void createMainDirectory() {
		File mainDir = new File(mainDirectory);
		//log("Is main dir exists: "+mainDir.exists()+"\n\n");
		if (!mainDir.exists()) {
			mainDir.mkdirs();
		}

	}

	private static void initializeDataStore(String fileName) throws IOException {
		FileInputStream inputStream = null;
		Scanner sc = null;
		try {
			inputStream = new FileInputStream(fileName);
			sc = new Scanner(inputStream, "UTF-8");

			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				//System.out.println(line);
				JSONObject json = new JSONObject(line);
				Iterator<String> keys = json.keys();
				if (keys.hasNext()) {
					dataStore.put((String) keys.next(), ++lineNo);
				}

			}
			if (sc.ioException() != null) {
				throw sc.ioException();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (sc != null) {
				sc.close();
			}
		}

	}

	private static File checkAndCreateDataStoreIfNotExist() {

		File file = null;
		try {
			file = new File(defaultDataDirectory + File.separator + "contacts.json");
			if (!file.exists()) {
				System.out.println("Creating file " + file.getPath());
				file.createNewFile();

			}
			double size = getFileSizeMegaBytes(file);
			// System.out.println(size+"mb");
			if (size > 1024) {
				log("File size Exceeded Thank you...");
				System.exit(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return file;
	}

	private static double getFileSizeMegaBytes(File file) {
		return (double) file.length() / (1024 * 1024);
	}

	public static void log(String str) {
		System.out.print(str);
	}

	public static String readFile(String fileName) throws Exception {
		File file = new File(fileName);
		//log("InfoFile length: "+file.length()+"\n");
		if(file.length() == 0) {
			//log(defaultDataDirectory+" default\n");
			return defaultDataDirectory;
		}
		//log(fileName+" fileName from readFile");
		BufferedReader br = null;
		try {
			StringBuffer content = new StringBuffer();
			String line = null;
			br = readFileAsStream(fileName);
			if (br != null) {
				while ((line = br.readLine()) != null) {
					content.append(line);
				}
			}
			//log(content.toString()+" read from file\n");
			return content.toString();
		} finally {
			if (br != null) {
				br.close();
			}
		}
	}

	public static BufferedReader readFileAsStream(String fileName) throws Exception {
		BufferedReader br = null;
		File file = new File(fileName);
		if (file.exists()) {
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
		}
		return br;
	}

}
